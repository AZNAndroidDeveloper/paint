package uz.azn.paint

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val canvasView = findViewById<CanvasView>(R.id.canvasView)
        val clear = findViewById<Button>(R.id.clear_button)
        clear.setOnClickListener {
            canvasView.ClearCanvas()
        }

    }
}